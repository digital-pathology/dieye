package ee.dieye.openslide;


import com.bc.ceres.glevel.MultiLevelModel;
import com.bc.ceres.glevel.MultiLevelSource;
import org.openslide.OpenSlide;

import javax.media.jai.PlanarImage;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class OpenSlideSource implements MultiLevelSource {

    private final OpenSlide slide;
    private final OpenSlideModel model;
    private Map<Integer, RenderedImage> levelImages;

    public OpenSlideSource(File slideFile) {
        try {
            this.slide = new OpenSlide(slideFile);
            this.model = new OpenSlideModel(this.slide);
            this.levelImages = new LinkedHashMap<>();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public MultiLevelModel getModel() {
        return model;
    }

    @Override
    public RenderedImage getImage(int level) {
        RenderedImage image = levelImages.get(level);
        if (image == null) {
            image = PlanarImage.wrapRenderedImage(new OpenSlideImage(slide, level));
            levelImages.put(level, image);
        }
        return image;
    }

    @Override
    public Shape getImageShape(int level) {
        int levelWidth = (int) slide.getLevelWidth(level);
        int levelHeight = (int) slide.getLevelHeight(level);
        return new Rectangle(levelWidth, levelHeight);
    }

    @Override
    public void reset() {

    }

    Map<String, String> getProperties() {
        return slide.getProperties();
    }
}
