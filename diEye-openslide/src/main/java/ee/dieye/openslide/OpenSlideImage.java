/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.dieye.openslide;


import org.apache.commons.lang3.reflect.FieldUtils;
import org.openslide.OpenSlide;

import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.util.Vector;

public class OpenSlideImage implements RenderedImage {
    public static final int TILE_WIDTH = 1024;
    public static final int TILE_HEIGHT = 1024;

    private final OpenSlide slide;
    private final int level;
    private final ColorModel colorModel;
    private final int width;
    private final int height;
    private final SampleModel sampleModel;

    public OpenSlideImage(OpenSlide slide, int level) {
        this.slide = slide;
        this.level = level;
        this.width = (int) slide.getLevelWidth(level);
        this.height = (int) slide.getLevelHeight(level);
        this.colorModel = ColorModel.getRGBdefault();
        try {
            int[] maskArray = (int[]) FieldUtils.readField(colorModel, "maskArray", true);
            this.sampleModel = CustomSampleModel.newSampleModel(colorModel.getTransferType(), width, height, maskArray);
        } catch (IllegalAccessException e) {
            throw new RuntimeException();
        }

//        this.sampleModel = this.colorModel.createCompatibleSampleModel(width, height);
    }

    @Override
    public Vector<RenderedImage> getSources() {
        return null;
    }

    @Override
    public Object getProperty(String name) {
        return slide.getProperties().get(name);
    }

    @Override
    public String[] getPropertyNames() {
        return (String[]) slide.getProperties().keySet().toArray();
    }

    @Override
    public ColorModel getColorModel() {
        return colorModel;
    }

    @Override
    public SampleModel getSampleModel() {
        return sampleModel;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getMinX() {
        return 0;
    }

    @Override
    public int getMinY() {
        return 0;
    }

    @Override
    public int getNumXTiles() {
        return (int) Math.ceil(getWidth() / (double) TILE_WIDTH);
    }

    @Override
    public int getNumYTiles() {
        return (int) Math.ceil(getHeight() / (double) TILE_HEIGHT);
    }

    @Override
    public int getMinTileX() {
        return 0;
    }

    @Override
    public int getMinTileY() {
        return 0;
    }

    @Override
    public int getTileWidth() {
        return TILE_WIDTH;
    }

    @Override
    public int getTileHeight() {
        return TILE_HEIGHT;
    }

    @Override
    public int getTileGridXOffset() {
        return 0;
    }

    @Override
    public int getTileGridYOffset() {
        return 0;
    }

    @Override
    public Raster getTile(int tileX, int tileY) {
        if (tileX < 0 || tileY < 0) {
            throw new IllegalArgumentException("Negative tile index");
        }
        if (tileX > getNumXTiles() || tileY > getNumYTiles()) {
            throw new IllegalArgumentException("Tile index out of bounds");
        }
        int startX = TILE_WIDTH * tileX;
        int startY = TILE_HEIGHT * tileY;
        return getRegion(startX, startY, TILE_WIDTH, TILE_HEIGHT);
    }

    @Override
    public Raster getData() {
        return getRegion(getMinX(), getMinY(), getWidth(), getHeight());
    }

    @Override
    public Raster getData(Rectangle rect) {
        //TODO add bounds validation
        return getRegion(rect.x, rect.y, rect.width, rect.height);
    }

    @Override
    public WritableRaster copyData(WritableRaster raster) {
        if (raster == null) {
            return (WritableRaster) getData();
        } else {
            return getRegion(raster.getMinX(), raster.getMinY(), raster.getWidth(), raster.getHeight());
        }
    }

    private WritableRaster getRegion(int startX, int startY, int width, int height) {
        
        double scale = slide.getLevelDownsample(level);
        long realX = (long) (startX * scale);
        long realY = (long) (startY * scale);
        WritableRaster writableRaster = colorModel.createCompatibleWritableRaster(width, height);
        int[] data = ((DataBufferInt) writableRaster.getDataBuffer()).getData();
        try {
            slide.paintRegionARGB(data, realX, realY, level, width, height);
            return writableRaster;
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /**
     * Quite ugly workaround to bypass nasty dimensions check in SampleModel class.
     * Our images may contain more pixels than Integer.MAX_VALUE.
     * Hope it does not break anything.
     * @see java.awt.image.SampleModel:129
     */
    static class CustomSampleModel extends SinglePixelPackedSampleModel {


        public CustomSampleModel(int dataType, int w, int h, int[] bitMasks, int realW, int realH) {
            super(dataType, w, h, bitMasks);
            width = realW;
            height = realH;
        }

        public CustomSampleModel(int dataType, int w, int h, int[] bitMasks) {
            super(dataType, w, h, bitMasks);
        }

        public static CustomSampleModel newSampleModel(int dataType, int w, int h, int[] bitMasks) {
            long size = (long)w * h;
            if (size >= Integer.MAX_VALUE) {
                return new CustomSampleModel(dataType, 100, 100, bitMasks, w, h);
            }
            return new CustomSampleModel(dataType, w, h, bitMasks);
        }
    }
}
