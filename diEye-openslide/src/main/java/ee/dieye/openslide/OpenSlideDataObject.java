package ee.dieye.openslide;


import ee.dieye.core.slide.SlideDataObject;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

@Messages({
    "LBL_OpenSlide_LOADER=OpenSlide files"
})
@MIMEResolver.ExtensionRegistration(
    displayName = "#LBL_OpenSlide_LOADER",
    mimeType = "image/openslide",
    extension = {"svs", "tif", "vms", "vmu", "ndpi", "scn", "mrxs", "tiff", "svslide", "bif"},
    position = 100
)
@DataObject.Registration(
    mimeType = "image/openslide",
//    iconBase = "org/stalv/dptoolbox/openslide/libreoffice-oasis-master-document.png",
    displayName = "#LBL_OpenSlide_LOADER",
    position = 300
)
@ActionReferences({
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
        position = 100,
        separatorAfter = 200
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
        position = 300
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
        position = 400,
        separatorAfter = 500
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
        position = 600
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
        position = 700,
        separatorAfter = 800
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
        position = 900,
        separatorAfter = 1000
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
        position = 1100,
        separatorAfter = 1200
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
        position = 1300
    )
    ,
    @ActionReference(
        path = "Loaders/image/openslide/Actions",
        id = @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
        position = 1400
    )
})
public class OpenSlideDataObject extends SlideDataObject<OpenSlideSource> {

  private OpenSlideSource source;

  public OpenSlideDataObject(FileObject pf, MultiFileLoader loader) throws  IOException {
    super(pf, loader);
    registerEditor("image/openslide", true);
    readOpenSlide(pf);
  }

  private void readOpenSlide(FileObject pf) {
    File slideFile = FileUtil.toFile(pf);
    this.source = new OpenSlideSource(slideFile);
  }

  @Override
  protected int associateLookup() {
    return 1;
  }

  @Override
  public OpenSlideSource getSlide() {
    return this.source;
  }

  @Override
  protected Node createNodeDelegate() {
    return new OpenSlideDataNode(this, Children.LEAF, getLookup());
  }


  class OpenSlideDataNode extends DataNode {

    public OpenSlideDataNode(final DataObject obj, final Children ch, final Lookup lookup) {
      super(obj, ch, lookup);
    }

    @Override
    public String getName() {
      return getDataObject().getPrimaryFile().getName();
    }

    @Override
    protected Sheet createSheet() {
      final Sheet sheet = super.createSheet();
      final Sheet.Set propertiesSet = Sheet.createPropertiesSet();
      OpenSlideSource slideSource = getSource();
      final Map<String, String> properties = new TreeMap<>(slideSource.getProperties());
      for (Map.Entry<String, String> property : properties.entrySet()) {
        propertiesSet.put(new StringProperty(property.getKey(), property.getValue()));
      }
      sheet.put(propertiesSet);
      return sheet;
    }

    private OpenSlideSource getSource() {
      return ((OpenSlideDataObject) getDataObject()).getSlide();
    }

    private class StringProperty extends PropertySupport.ReadOnly<String> {

      private final String value;

      public StringProperty(final String name, final String value) {
        super(name, String.class, name, name);
        this.value = value;
      }

      @Override
      public String getValue() {
        return value;
      }
    }
  }
}
