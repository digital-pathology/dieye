package ee.dieye.openslide;


import com.bc.ceres.glevel.MultiLevelModel;
import org.openslide.OpenSlide;

import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Rectangle2D;

public class OpenSlideModel implements MultiLevelModel {


    private final OpenSlide slide;

    public OpenSlideModel(OpenSlide slide) {
        this.slide = slide;
    }

    @Override
    public int getLevelCount() {
        return slide.getLevelCount();
    }

    @Override
    public int getLevel(double scale) {
        return slide.getBestLevelForDownsample(scale);
    }

    @Override
    public double getScale(int level) {
        return slide.getLevelDownsample(level);
    }
     
    @Override
    public AffineTransform getImageToModelTransform(int level) {
        AffineTransform transform = new AffineTransform();
        transform.scale(getScale(level), getScale(level));
        return transform;
    }

    @Override
    public AffineTransform getModelToImageTransform(int level) {

        try {
            return getImageToModelTransform(level).createInverse();
        } catch (NoninvertibleTransformException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public Rectangle2D getModelBounds() {
        return new Rectangle(0, 0, (int) slide.getLevel0Width(), (int) slide.getLevel0Height());
    }
}
