package ee.dieye.core.slide;

import com.bc.ceres.swing.figure.Figure;
import com.bc.ceres.swing.figure.FigureFactory;
import com.bc.ceres.swing.figure.ShapeFigure;
import com.bc.ceres.swing.figure.support.DefaultFigureFactory;
import com.bc.ceres.swing.figure.support.DefaultFigureStyle;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ee.dieye.core.annotation.Annotation;
import ee.dieye.core.annotation.serialization.FigureDeserializer;
import ee.dieye.core.annotation.serialization.FigureSerializer;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.IOException;

import static org.testng.Assert.*;

public class SlideMetadataSerializationTest {

  private ObjectMapper objectMapper;
  private FigureFactory figureFactory = new DefaultFigureFactory();

  // TODO move to test resources as json
  private static final String METADATA =
      "{\"annotations\":[{\"figure\":{\"rank\":2,\"normalStyle\":\"fill:#0000ff; stroke:#00ff00\",\"path\":[{\"segment\":0,\"coords\":[20.0,30.0,0.0,0.0,0.0,0.0]},{\"segment\":1,\"coords\":[220.0,30.0,0.0,0.0,0.0,0.0]},{\"segment\":1,\"coords\":[220.0,130.0,0.0,0.0,0.0,0.0]},{\"segment\":1,\"coords\":[20.0,130.0,0.0,0.0,0.0,0.0]},{\"segment\":1,\"coords\":[20.0,30.0,0.0,0.0,0.0,0.0]},{\"segment\":4,\"coords\":[20.0,30.0,0.0,0.0,0.0,0.0]}]},\"description\":\"Annotation rectangle\"}]}";

  @BeforeMethod
  public void setUp() {
    objectMapper = new ObjectMapper();
    SimpleModule module = new SimpleModule();
    module.addSerializer(new FigureSerializer());
    module.addDeserializer(Figure.class, new FigureDeserializer());
    objectMapper.registerModule(module);
  }

  @Test
  public void testSerialization() throws JsonProcessingException {
    SlideMetadata metadata = new SlideMetadata();
    final ShapeFigure rectangle =
        figureFactory.createPolygonFigure(
            new Rectangle(20, 30, 200, 100),
            DefaultFigureStyle.createPolygonStyle(Color.BLUE, Color.GREEN));
    Annotation annotationRectangle = new Annotation(rectangle, "Annotation rectangle");
    metadata.getAnnotations().add(annotationRectangle);
    String serializedString = objectMapper.writeValueAsString(metadata);
    assertNotNull(serializedString);
  }

  @Test
  public void testDeserialization() throws IOException {
    final SlideMetadata metadata = objectMapper.readValue(METADATA, SlideMetadata.class);
    assertNotNull(metadata);
    assertEquals(
        metadata.getAnnotations().stream().findFirst().get().getDescription(),
        "Annotation rectangle");
  }
}
