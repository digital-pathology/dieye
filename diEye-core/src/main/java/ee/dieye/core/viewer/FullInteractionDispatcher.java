package ee.dieye.core.viewer;

import com.bc.ceres.swing.figure.InteractionDispatcher;
import com.bc.ceres.swing.figure.Interactor;
import com.bc.ceres.swing.figure.InteractorAware;

import javax.swing.JComponent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class FullInteractionDispatcher extends InteractionDispatcher implements MouseWheelListener {

  private final InteractorAware interactorAware;

  public FullInteractionDispatcher(final InteractorAware interactorAware) {
    super(interactorAware);
    this.interactorAware = interactorAware;
  }

  @Override
  public void registerListeners(final JComponent component) {
    super.registerListeners(component);
    component.addMouseWheelListener(this);
  }

  @Override
  public void unregisterListeners(final JComponent component) {
    super.unregisterListeners(component);
    component.removeMouseWheelListener(this);
  }

  @Override
  public void mouseWheelMoved(final MouseWheelEvent event) {

    final Interactor interactor = getInteractor();
    if (interactor.isActive() && interactor instanceof MouseWheelListener) {
      MouseWheelListener listener = (MouseWheelListener) interactor;
      listener.mouseWheelMoved(event);
    }
  }

  private Interactor getInteractor() {
    return interactorAware.getInteractor();
  }
}
