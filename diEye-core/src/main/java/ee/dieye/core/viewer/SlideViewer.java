package ee.dieye.core.viewer;

import com.bc.ceres.glayer.support.ImageLayer;
import com.bc.ceres.glayer.swing.LayerCanvas;
import com.bc.ceres.glevel.MultiLevelSource;
import com.bc.ceres.swing.figure.*;
import com.bc.ceres.swing.figure.interactions.*;
import org.openide.util.ImageUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class SlideViewer extends JPanel implements FigureEditorAware {

    private final JToolBar toolBar;
    private final LayerCanvas layerCanvas;
    private final AnnotationEditor figureEditor;


    private final Interactor selectionInteractor = new SelectionInteractor();
    private final Interactor lineInteractor = new InsertLineFigureInteractor();
    private final Interactor rectangleInteractor = new InsertRectangleFigureInteractor();
    private final Interactor ellipseInteractor = new InsertEllipseFigureInteractor();
    private final Interactor polylineInteractor = new InsertPolylineFigureInteractor();
    private final Interactor polygoneInteractor = new InsertPolygonFigureInteractor();
    private final PanAndZoomInteractor panAndZoomInteractor = new PanAndZoomInteractor();

    private AbstractButton analysisButton;

    private final ImageLayer baseImage;

    public SlideViewer(MultiLevelSource source) {
        super(new BorderLayout(), true);
        this.baseImage = new ImageLayer(source);
        this.layerCanvas = new LayerCanvas(baseImage);
        layerCanvas.setBackground(Color.WHITE); //TODO get background from WSI source
        add(layerCanvas, BorderLayout.CENTER);
        this.figureEditor = new AnnotationEditor(layerCanvas);
        figureEditor.setInteractor(panAndZoomInteractor);

        layerCanvas.addKeyListener(new AbstractInteractor() {
            @Override
            public void keyPressed(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.VK_DELETE ) {
                    figureEditor.getSelectionContext().deleteSelection();
                }
            }
        });
        layerCanvas.addOverlay((canvas, rendering) -> figureEditor.draw(rendering));
        this.toolBar = new JToolBar();

        initToolbar(toolBar);
    }

    private void initToolbar(final JToolBar toolbar) {
        AbstractButton navigateButton = createToolButton(figureEditor, "Navigate", "ee/dieye/core/icons/ZoomTool24.gif", panAndZoomInteractor);
        navigateButton.setSelected(true);
        AbstractButton selectionButton = createToolButton(figureEditor, "Select", "ee/dieye/core/icons/SelectTool24.gif", selectionInteractor);
        AbstractButton lineButton = createToolButton(figureEditor, "Line", "ee/dieye/core/icons/DrawLineTool24.gif", lineInteractor);
        AbstractButton rectangleButton = createToolButton(figureEditor, "Rectangle", "ee/dieye/core/icons/DrawRectangleTool24.gif", rectangleInteractor);
        AbstractButton ellipseButton = createToolButton(figureEditor, "Ellipse", "ee/dieye/core/icons/DrawEllipseTool24.gif", ellipseInteractor);
        AbstractButton polylineButton = createToolButton(figureEditor, "Polyline", "ee/dieye/core/icons/DrawPolylineTool24.gif", polylineInteractor);
        AbstractButton polygonButton = createToolButton(figureEditor, "Polygone", "ee/dieye/core/icons/DrawPolygonTool24.gif", polygoneInteractor);

        analysisButton = new JButton(ImageUtilities.loadImageIcon("ee/dieye/core/icons/ImageView24.gif", false));

        toolbar.add(selectionButton);
        toolbar.add(navigateButton);
        toolbar.addSeparator();
        toolbar.add(lineButton);
        toolbar.add(rectangleButton);
        toolbar.add(ellipseButton);
        toolbar.add(polylineButton);
        toolbar.add(polygonButton);
        toolbar.addSeparator();
        toolbar.add(analysisButton);


        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(navigateButton);
        buttonGroup.add(selectionButton);
        buttonGroup.add(lineButton);
        buttonGroup.add(rectangleButton);
        buttonGroup.add(ellipseButton);
        buttonGroup.add(polylineButton);
        buttonGroup.add(polygonButton);
    }

    private AbstractButton createToolButton(FigureEditor editor, String name, String iconFile, Interactor interactor) {
        ImageIcon icon = ImageUtilities.loadImageIcon(iconFile, false);
        final AbstractButton button = new JToggleButton(icon);
        button.setToolTipText(name);
        button.setSelected(false);
        button.addActionListener(e -> {
            if (button.isSelected()) {
                editor.setInteractor(interactor);
            }
        });
        interactor.addListener(new AbstractInteractorListener() {
            @Override
            public void interactorActivated(Interactor interactor) {
                button.setSelected(true);
            }

            @Override
            public void interactorDeactivated(Interactor interactor) {
                button.setSelected(false);
            }
        });
        return button;
    }

    public FigureEditor getFigureEditor() {
        return figureEditor;
    }

    public JToolBar getToolBar() {
        return toolBar;
    }

    public AbstractButton getAnalysisButton() {
        return analysisButton;
    }


}

