package ee.dieye.core.viewer;

import com.bc.ceres.grender.Viewport;
import com.bc.ceres.swing.figure.ViewportInteractor;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class PanAndZoomInteractor extends ViewportInteractor implements MouseWheelListener {

  private int viewportX;
  private int viewportY;

  @Override
  public void mousePressed(MouseEvent event) {
    viewportX = event.getX();
    viewportY = event.getY();
  }

  @Override
  public void mouseDragged(MouseEvent event) {
    Viewport viewport = getViewport(event);
    int viewportX = event.getX();
    int viewportY = event.getY();
    final double dx = viewportX - this.viewportX;
    final double dy = viewportY - this.viewportY;
    viewport.moveViewDelta(dx, dy);
    this.viewportX = viewportX;
    this.viewportY = viewportY;
  }

  @Override
  public void mouseWheelMoved(MouseWheelEvent e) {
    final int wheelRotation = e.getWheelRotation();
    final Viewport viewport = getViewport(e);
    final double newZoomFactor = viewport.getZoomFactor() * Math.pow(1.1, wheelRotation);
    viewport.setZoomFactor(newZoomFactor);
  }
}
