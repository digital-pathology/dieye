package ee.dieye.core.annotation;

import com.bc.ceres.swing.figure.Figure;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Annotation {

  private final Figure figure;
  private final String description;

  @JsonCreator
  public Annotation(@JsonProperty("figure") Figure figure, @JsonProperty("description") String description) {
    this.figure = figure;
    this.description = description;
  }

  public Figure getFigure() {
    return figure;
  }

  public String getDescription() {
    return description;
  }
}
