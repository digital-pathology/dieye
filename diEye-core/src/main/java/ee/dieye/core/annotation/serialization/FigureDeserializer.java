package ee.dieye.core.annotation.serialization;

import com.bc.ceres.swing.figure.Figure;
import com.bc.ceres.swing.figure.support.DefaultFigureFactory;
import com.bc.ceres.swing.figure.support.DefaultFigureStyle;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.awt.geom.Path2D;
import java.io.IOException;
import java.util.Iterator;

public class FigureDeserializer extends StdDeserializer<Figure> {
  public FigureDeserializer() {
    super(Figure.class);
  }

  @Override
  public Figure deserialize(final JsonParser p, final DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    JsonNode node = p.getCodec().readTree(p);
    final int rank = node.get("rank").asInt();
    final String normalStyle = node.get("normalStyle").asText();
    final Iterator<JsonNode> path = node.get("path").elements();
    Path2D.Double path2d = new Path2D.Double();
    path2d.append(new FigurePathIterator(path), false);
    DefaultFigureFactory factory = new DefaultFigureFactory();
    DefaultFigureStyle style = new DefaultFigureStyle();
    style.fromCssString(normalStyle);
    if (rank == Figure.Rank.AREA.value) {
      return factory.createPolygonFigure(path2d, style);
    } else {
      return factory.createLineFigure(path2d, style);
    }
  }
}
