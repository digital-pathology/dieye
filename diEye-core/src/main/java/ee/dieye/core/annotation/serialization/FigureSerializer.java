package ee.dieye.core.annotation.serialization;

import com.bc.ceres.swing.figure.Figure;
import com.bc.ceres.swing.figure.support.DefaultShapeFigure;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.io.IOException;

public class FigureSerializer extends StdSerializer<Figure> {

  public FigureSerializer() {
    this(Figure.class);
  }

  public FigureSerializer(final Class<Figure> t) {
    super(t);
  }

  @Override
  public void serialize(
      final Figure value, final JsonGenerator gen, final SerializerProvider provider)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("rank", value.getRank().value);
    gen.writeStringField("normalStyle", value.getNormalStyle().toCssString());
    final DefaultShapeFigure value1 = (DefaultShapeFigure) value;
    final PathIterator pathIterator = value1.getShape().getPathIterator(new AffineTransform());
    double coords[] = new double[6];
    gen.writeArrayFieldStart("path");
    while (!pathIterator.isDone()) {
      gen.writeStartObject();
      final int segment = pathIterator.currentSegment(coords);
      gen.writeNumberField("segment", segment);
      gen.writeFieldName("coords");
      gen.writeArray(coords, 0, 6);
      gen.writeEndObject();
      pathIterator.next();
    }
    gen.writeEndArray();
    gen.writeEndObject();
  }
}
