package ee.dieye.core.annotation.serialization;

import com.fasterxml.jackson.databind.JsonNode;

import java.awt.geom.PathIterator;
import java.util.Iterator;

public class FigurePathIterator implements PathIterator {

  private final Iterator<JsonNode> path;
  private JsonNode currentNode;

  public FigurePathIterator(final Iterator<JsonNode> path) {
    this.path = path;
    this.currentNode = path.next();
  }

  @Override
  public int getWindingRule() {
    return 0;
  }

  @Override
  public boolean isDone() {
    return !path.hasNext();
  }

  @Override
  public void next() {
    this.currentNode = path.next();
  }

  @Override
  public int currentSegment(float[] coords) {
    int segment = currentNode.get("segment").asInt();
    Iterator<JsonNode> elements = currentNode.get("coords").elements();
    for (int i = 0; i < 6; ++i) {
      coords[i] = elements.next().floatValue();
    }
    return segment;
  }

  @Override
  public int currentSegment(double[] coords) {
    int segment = currentNode.get("segment").asInt();
    Iterator<JsonNode> elements = currentNode.get("coords").elements();
    for (int i = 0; i < 6; ++i) {
      coords[i] = elements.next().asDouble();
    }
    return segment;
  }
}
