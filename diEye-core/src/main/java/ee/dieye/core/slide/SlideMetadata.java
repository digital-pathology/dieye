package ee.dieye.core.slide;

import ee.dieye.core.annotation.Annotation;

import java.util.ArrayList;
import java.util.List;

public class SlideMetadata {

  private List<Annotation> annotations = new ArrayList<>();

  public SlideMetadata() {}

  public List<Annotation> getAnnotations() {
    return annotations;
  }
}
