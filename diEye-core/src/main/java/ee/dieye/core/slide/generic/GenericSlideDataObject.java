package ee.dieye.core.slide.generic;

import com.bc.ceres.glevel.support.DefaultMultiLevelSource;
import ee.dieye.core.slide.SlideDataObject;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.NbBundle;

import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@NbBundle.Messages({
    "LBL_GenericSlide_LOADER=Generic slide images"
})
@MIMEResolver.ExtensionRegistration(
    displayName = "#LBL_GenericSlide_LOADER",
    mimeType = "image/genericslide",
    extension = {"jpg", "jpeg", "png", "bmp"},
    position = 100
)
@DataObject.Registration(
    mimeType = "image/genericslide",
    iconBase = "ee/dieye/core/libreoffice-oasis-master-document.png",
    displayName = "#LBL_GenericSlide_LOADER",
    position = 300
)

public class GenericSlideDataObject extends SlideDataObject<DefaultMultiLevelSource> {

  private DefaultMultiLevelSource source;

  public GenericSlideDataObject(final FileObject fo, final MultiFileLoader loader)
      throws DataObjectExistsException {
    super(fo, loader);
    registerEditor("image/genericslide", true);
    loadSlide(fo);
  }

  private void loadSlide(final FileObject fo) {
    try {
      File slideFile = FileUtil.toFile(fo);
      final BufferedImage image = ImageIO.read(slideFile);
      source = new DefaultMultiLevelSource(PlanarImage.wrapRenderedImage(image), 1);
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  @Override
  public DefaultMultiLevelSource getSlide() {
    return source;
  }

  @Override
  protected int associateLookup() {
    return 1;
  }
}
