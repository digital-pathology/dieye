package ee.dieye.core.slide;

import com.bc.ceres.glevel.MultiLevelSource;
import com.bc.ceres.swing.figure.Figure;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ee.dieye.core.annotation.serialization.FigureDeserializer;
import ee.dieye.core.annotation.serialization.FigureSerializer;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;

import java.io.IOException;

public abstract class SlideDataObject<T extends MultiLevelSource> extends MultiDataObject {
  private static final String METADATA_EXT = "metadata";

  private SlideMetadata metadata;
  private final ObjectMapper objectMapper = new ObjectMapper();

  public SlideDataObject(final FileObject fo, final MultiFileLoader loader)
      throws DataObjectExistsException {
    super(fo, loader);
    registerMapperModules();
    loadMetadata(fo);
  }

  private void loadMetadata(FileObject fo) {
    final String name = fo.getName();
    for (FileObject fileObject : fo.getParent().getChildren()) {
      if (fileObject.getName().equalsIgnoreCase(name) && fileObject.hasExt(METADATA_EXT)) {
        try {
          metadata = objectMapper.readValue(fileObject.getInputStream(), SlideMetadata.class);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  protected void registerMapperModules() {
    SimpleModule module = new SimpleModule();
    module.addSerializer(new FigureSerializer());
    module.addDeserializer(Figure.class, new FigureDeserializer());
    objectMapper.registerModule(module);
  }

  public abstract T getSlide();

  public SlideMetadata getMetadata() {
    return metadata;
  }
}
